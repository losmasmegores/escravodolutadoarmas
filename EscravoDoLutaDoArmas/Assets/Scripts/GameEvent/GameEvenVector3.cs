using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameEventVector3")]
public class GameEventVector3 : ScriptableObject
{
    private readonly List<GameEventListenerVector3> eventListeners =
        new List<GameEventListenerVector3>();

    public void Raise(Vector3 a)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(a);
    }

    public void RegisterListener(GameEventListenerVector3 listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListenerVector3 listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
