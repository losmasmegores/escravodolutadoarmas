using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class InventarioSlaves : ScriptableObject
{

    public ScriptableSlave[] ListaEscalvos;
        
}
