using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ImageColor : MonoBehaviour
{
    private Image Colorines;
    [SerializeField]
    private TextMeshProUGUI textaco;
    void Awake()
    {
        Colorines = transform.GetComponent<Image>();   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setColor(Player p)
    {
        switch (p)
        {
            case Player.Player1:
                Colorines.color = Color.blue;
                break; 
            case Player.Player2:
                Colorines.color = Color.red;
                break;
        }
    }
    public void setText(Player p)
    {
        switch (p)
        {
            case Player.Player1:
                textaco.text = "Player 1";
                break;
            case Player.Player2:
                textaco.text = "Player 2";
                break;
        }
    }
}
