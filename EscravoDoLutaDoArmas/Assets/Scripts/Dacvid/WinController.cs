using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinController : MonoBehaviour
{
    [SerializeField]
    private ScriptableSelector naboDeCUlo;

    private static int ronda = 1;
    private static int esclavo = 1;

    [SerializeField] private GameEventInt GameEventInt;

    private void Awake()
    {
        for (int i = 0; i < naboDeCUlo.Warriors.Length; i++)
        {
            naboDeCUlo.Warriors[i].Botas = false;
            naboDeCUlo.Warriors[i].Arma = false;
            naboDeCUlo.Warriors[i].Casco = false;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (esclavo != 4)
        {
            GameEventInt.Raise(ronda, esclavo);
            
            collision.transform.position = new Vector3(0, 0, -1);
            collision.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

            Debug.Log(ronda);
            if (ronda == 3)
            {
                naboDeCUlo.Warriors[esclavo-1].Botas = true;
                ronda = 1;
                esclavo++;
            }
            else if (ronda == 2)
            {
                naboDeCUlo.Warriors[esclavo-1].Arma = true;
                ronda++;
            }
            else if (ronda == 1)
            {
                naboDeCUlo.Warriors[esclavo-1].Casco = true;
                ronda++;
            }
        }  
        else SceneManager.LoadScene("Game");
    }
}
