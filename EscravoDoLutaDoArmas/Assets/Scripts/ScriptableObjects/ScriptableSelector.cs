using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableSelector", menuName = "Scriptable Objects/Scriptable Selector")]
[Serializable]
public class ScriptableSelector : ScriptableObject
{
    [SerializeField]
    private ScriptableSlave[] warriors;

    public ScriptableSlave[] Warriors => warriors;
}
