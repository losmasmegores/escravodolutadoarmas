using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameEventSlave")]
public class GameEventSlave : ScriptableObject
{
    private readonly List<GameEventListenerSlave> eventListeners =
        new List<GameEventListenerSlave>();

    public void Raise(ScriptableSlave a)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(a);
    }

    public void RegisterListener(GameEventListenerSlave listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListenerSlave listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
