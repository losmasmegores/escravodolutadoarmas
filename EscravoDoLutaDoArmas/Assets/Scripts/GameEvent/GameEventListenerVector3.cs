using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventListenerVector3 : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GameEventVector3 Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent<Vector3> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(Vector3 a)
    {

        Response.Invoke(a);
    }
}
