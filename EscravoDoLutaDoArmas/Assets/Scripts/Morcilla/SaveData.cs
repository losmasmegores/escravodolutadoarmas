using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SaveData;

[Serializable]
public class SaveData
{
    [Serializable]
    public struct SlaveData
    {
        public double speed;
        public double dmg;
        public double vida;
        public bool casco;
        public bool arma;
        public bool botas;
        public string uniqueID;

        public SlaveData(double _spd, double _dmg, double _hp, bool _casco, bool _arma, bool _botas, string _id)
        {
            this.speed = _spd;
            this.dmg = _dmg;
            this.vida = _hp;
            this.casco = _casco;
            this.arma = _arma;
            this.botas = _botas;
            this.uniqueID = _id;
        }
    }

    public SlaveData[] m_Slaves;

    public void PopulateData(ISaveableObject[] _slavesData)
    {
        m_Slaves = new SlaveData[_slavesData.Length];
        for (int i = 0; i < _slavesData.Length; i++)
            m_Slaves[i] = _slavesData[i].Save();
    }
}

public interface ISaveableObject
{
    public SlaveData Save();
    public void Load(SlaveData _slaveData);
}

